# Environment Modules for Wheaton College HPC #

Environment Modules are a powerful and dynamic way to modify a user's environment during an active session as opposed to strictly at session start (as with a modification to their ~/.bashrc). The [original implmentation](http://modules.sourceforge.net/) (and still widely used version) of Environment Modules was written in TCL. Wheaton College uses the more modern [Lmod](https://www.tacc.utexas.edu/research-development/tacc-projects/lmod) implementation of the environment odules concept distributed by [TACC](https://www.tacc.utexas.edu/) and packaged by the [OpenHPC project](https://openhpc.community/). We've chosen this implementation for 2 reasons:

* It uses lua, which is a friendlier language than TCL.
* It can do software heirarchy, thereby making it harder (impossible?) for a user to load an incompatible set of modules.

We've based our implementation of Lmod software hierarchy on:

* Jeff Layton's [article about Lmod](http://www.admin-magazine.com/HPC/Articles/Lmod-Alternative-Environment-Modules)
* The Lmod documentation on [Software Module hierarchy](http://lmod.readthedocs.io/en/latest/080_hierarchy.html)

### Sysadmin Setup ###

modulefiles from the mf directory are deployed to `/data/modulefiles/mf` and supplement the modulefiles that come with OpenHPC packages, which live in `/opt/ohpc/pub/module{files,deps}`.

The lmod-ohpc package comes with a profile.d script to configure the environment for lmod usage. We append a line to this file to pick up our additional modulefiles.

* `/etc/profile.d/lmod.sh`

### Development Workflow ###

1. Make all changes for new modulefiles in a named branch within your local clone of the repository.
2. Test your changes via `module use /path/to/repository/clone/mf` followed by `module load newmodname`.
3. Merge the branch into the master branch.
4. Push the changes to the [canonical copy](https://bitbucket.org/wheatoncollegeil/modulefiles) of the repository in Bitbucket.
5. Deploy the changes by pushing to hpc (see below).

### Deploying Updates ###

The process we're following is [documented online](https://www.digitalocean.com/community/tutorials/how-to-set-up-automatic-deployment-with-git-with-a-vps). There are four locations involved.

* The sysadmin's local clone of the modulefiles git repository.
* The [canonical version](https://bitbucket.org/wheatoncollegeil/modulefiles) of the repository in Bitbucket. All local changes should be pushed to this repository.
* `/data/src/repo/modulefiles.git` is a [bare git repository](http://www.saintsjd.com/2011/01/what-is-a-bare-git-repository/)
    * This should be set up as an additional remote for the sysadmin's local repository clone with `git remote add hpc ssh://user@hpc.wheaton.edu/data/src/repo/modulefiles.git`.
    * This repository has a post-receive hook (`/data/srv/repo/modulefiles.git/hooks/post-receive`) that will deploy any changes to the master branch into `/data/modulefiles`. Note that the deploy process does a sparse checkout that only includes README.md and the mf sub-directory.
* `/data/modulefiles` is the production copy of the modulefiles that Lmod on all cslab and HPC nodes are configured to use.
    * NOTE: the actual module files live in the `/data/modulefiles/mf` subdirectory.

With the above setup, deploying validated changes to modulefiles is as simple as `git push hpc master`.

### Additional help? ###

* [Neile Havens](mailto://neile.havens@wheaton.edu)
* [Lmod documentation](http://lmod.readthedocs.io/en/latest/)
* [module usage documentation](https://confluence.wheaton.edu/display/SC/Environment+Modules)

