local pkgName  = myModuleName()

whatis("Name: "..pkgName)
whatis("Description: The free version of CHARMM as well as the MMTSB toolset")
whatis("charmm documentation: https://www.charmm.org/charmm/documentation/")
whatis("MMTSB toolset documentation: http://blue11.bch.msu.edu/mmtsb/Main_Page")

help([[

Run 'module help charmm' for links to documentation.

]])

prepend_path("PATH", "/data/apps/charmm/bin")
setenv("MMTSBDIR", "/data/apps/mmtsb")
prepend_path("PATH", "/data/apps/mmtsb/perl")
prepend_path("PATH", "/data/apps/mmtsb/bin")
setenv("CHARMMEXEC", "/data/apps/charmm/bin/charmm")

depends_on("prun", "pmix", "gnu7/7.3.0", "openmpi3/3.1.0")
