local pkgName = myModuleName()

whatis("Name: "..pkgName)
whatis("Description: COMSOL Multiphysics is a general-purpose software platform, based on advanced numerical methods, for modeling and simulating physics-based problems.")
whatis("Documentation: https://www.comsol.com/support")
whatis("License: 2 concurrent users")

prepend_path("PATH", "/data/apps/comsol53/multiphysics/bin")
