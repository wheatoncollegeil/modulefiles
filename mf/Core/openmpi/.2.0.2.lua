prepend_path{"PATH","/data/apps/openmpi/2.0.2/bin",delim=":",priority="0"}
prepend_path{"LD_LIBRARY_PATH","/data/apps/openmpi/2.0.2/lib",delim=":",priority="0"}
prepend_path{"MANPATH","/data/apps/openmpi/2.0.2/share/man",delim=":",priority="0"}

setenv("MPI_DIR","/data/apps/openmpi/2.0.2")
setenv("MPI_HOME","/data/apps/openmpi/2.0.2")
setenv("MPI_BIN","/data/apps/openmpi/2.0.2/bin")
setenv("MPI_SYSCONFIG","/data/apps/openmpi/2.0.2/etc")
setenv("MPI_INCLUDE","/data/apps/openmpi/2.0.2/include")
setenv("MPI_LIB","/data/apps/openmpi/2.0.2/lib")
setenv("MPI_MAN","/data/apps/openmpi/2.0.2/share/man")

family("MPI")
depends_on("gnu/5.4.0")
