local pkgName     = myModuleName()
local fullVersion = myModuleVersion()

whatis("Name: "..pkgName)
whatis("Version: "..fullVersion)
whatis("Description: Intel FORTRAN compiler")

local baseDir = "/data/apps/intel"
local compilersAndLibsDir = pathJoin(baseDir, "compilers_and_libraries_2017.4.196")
local debuggerDir = pathJoin(baseDir, "debuger_2017")
local docsDir = pathJoin(baseDir, "documentation_2017")

prepend_path("CLASSPATH",compilersAndLibsDir.."/linux/mpi/intel64/lib/mpi.jar")
setenv("CPATH",compilersAndLibsDir.."/linux/mkl/include")
prepend_path("INFOPATH",docsDir.."/en/debugger/gdb-ia/info/:"..docsDir.."/en/debugger//gdb-mic/info/")
setenv("INTEL_LICENSE_FILE",compilersAndLibsDir.."/linux/licenses")

setenv("GDBSERVER_MIC",debuggerDir.."/gdb/targets/mic/bin/gdbserver")
setenv("GDB_CROSS",debuggerDir.."/gdb/intel64_mic/bin/gdb-mic")
setenv("INTEL_PYTHONHOME",debuggerDir.."/python/intel64/")
setenv("MPM_LAUNCHER",debuggerDir.."/mpm/mic/bin/start_mpm.sh")

setenv("I_MPI_ROOT",compilersAndLibsDir.."/linux/mpi")
setenv("MIC_LD_LIBRARY_PATH",compilersAndLibsDir.."/linux/mpi/mic/lib:"..compilersAndLibsDir.."/linux/compiler/lib/mic:"..compilersAndLibsDir.."/linux/compiler/lib/intel64_lin_mic:"..compilersAndLibsDir.."/linux/mkl/lib/intel64_lin_mic")
setenv("MIC_LIBRARY_PATH",compilersAndLibsDir.."/linux/mpi/mic/lib:"..compilersAndLibsDir.."/linux/compiler/lib/mic:"..compilersAndLibsDir.."/linux/compiler/lib/intel64_lin_mic:"..compilersAndLibsDir.."/linux/mkl/lib/intel64_lin_mic")
setenv("MKLROOT",compilersAndLibsDir.."/linux/mkl")
setenv("NLSPATH",compilersAndLibsDir.."/linux/compiler/lib/intel64/locale/%l_%t/%N:"..compilersAndLibsDir.."/linux/mkl/lib/intel64_lin/locale/%l_%t/%N:/data/apps/intel/debugger_2017/gdb/intel64_mic/share/locale/%l_%t/%N:/data/apps/intel/debugger_2017/gdb/intel64/share/locale/%l_%t/%N")

prepend_path("LD_LIBRARY_PATH",compilersAndLibsDir.."/linux/compiler/lib/intel64")
prepend_path("LD_LIBRARY_PATH",compilersAndLibsDir.."/linux/compiler/lib/intel64_lin")
prepend_path("LD_LIBRARY_PATH",compilersAndLibsDir.."/linux/mpi/intel64/lib")
prepend_path("LD_LIBRARY_PATH",compilersAndLibsDir.."/linux/mpi/mic/lib")
prepend_path("LD_LIBRARY_PATH",compilersAndLibsDir.."/linux/compiler/lib/intel64_lin:"..compilersAndLibsDir.."/linux/mkl/lib/intel64_lin:/data/apps/intel/debugger_2017/libipt/intel64/lib")
prepend_path("LIBRARY_PATH",compilersAndLibsDir.."/linux/compiler/lib/intel64_lin:"..compilersAndLibsDir.."/linux/mkl/lib/intel64_lin")
prepend_path("MANPATH","/data/apps/intel/man/common:"..compilersAndLibsDir.."/linux/mpi/man:"..docsDir.."/en/debugger//gdb-ia/man/:"..docsDir.."/en/debugger//gdb-mic/man/:")
prepend_path("PATH",debuggerDir.."/gdb/intel64_mic/bin")
prepend_path("PATH",compilersAndLibsDir.."/linux/mpi/intel64/bin")
prepend_path("PATH",compilersAndLibsDir.."/linux/bin/intel64")

-- Setup Modulepath for packages built by this compiler
prepend_path("MODULEPATH", "/data/modulefiles/mf/Compiler/intel/2017.4") 

family("compiler")
