local pkgName     = myModuleName()
local fullVersion = myModuleVersion()

whatis("Name: "..pkgName)
whatis("Version: "..fullVersion)
whatis("Description: GROMACS is a versatile molecular dynamics package that "..
       "simulates the Newtonian equations of motion for systems with "..
       "hundreds to millions of particles.")
whatis("Package documentation: http://manual.gromacs.org/documentation/2016.5")

help([[

See the man pages for gmx and gmx-* for detailed usage information.

Version: 2016.5

]])

setenv("GMXBIN","/data/apps/gromacs-2016.5/bin")
setenv("GMXDATA","/data/apps/gromacs-2016.5/share/gromacs")
setenv("GMXLDLIB","/data/apps/gromacs-2016.5/lib64")
setenv("GMXMAN","/data/apps/gromacs-2016.5/share/man")
setenv("GROMACS_DIR","/data/apps/gromacs-2016.5")
prepend_path("LD_LIBRARY_PATH","/data/apps/gromacs-2016.5/lib64")
prepend_path("MANPATH","/data/apps/gromacs-2016.5/share/man")
prepend_path("PATH","/data/apps/gromacs-2016.5/bin")
prepend_path("PKG_CONFIG_PATH","/data/apps/gromacs-2016.5/lib64/pkgconfig")

depends_on("prun", "pmix", "gnu7/7.3.0", "openblas/0.2.20", "openmpi3/3.1.0", "scalapack/2.0.2")
