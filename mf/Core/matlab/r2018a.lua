help([[
Some help files are installed locally at file:///source/MATLAB/R2018a/help/matlab/index.html

Additional resources can be found online at https://www.mathworks.com/support/
]])

whatis("Name: MATLAB")
whatis("Version: R2018a")
whatis("Description: Setup environment for MATLAB usage")
whatis("Documentation: https://www.mathworks.com/support/")

prepend_path("PATH", "/source/MATLAB/R2018a/bin")
