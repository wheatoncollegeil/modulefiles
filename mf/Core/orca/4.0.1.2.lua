local pkgName     = myModuleName()
local fullVersion = myModuleVersion()

whatis("Name: "..pkgName)
whatis("Version: "..fullVersion)
whatis("Description: ORCA is an ab initio, DFT and semiempirical SCF-MO package."..
       "simulates the Newtonian equations of motion for systems with "..
       "hundreds to millions of particles.")
whatis("Package documentation: https://orcaforum.cec.mpg.de/")

help([[

See the manual in the same folder as the binaries:
  /data/apps/orca_4_0_1_2_linux_x86-64_openmpi202/orca_manual_4_0_1.pdf

Version: 4.0.1.2

]])

prepend_path("PATH","/data/apps/orca_4_0_1_2_linux_x86-64_openmpi202")
setenv("ORCAHOME","/data/apps/orca_4_0_1_2_linux_x86-64_openmpi202")

depends_on("prun", "gnu/5.4.0", "openmpi/.2.0.2")
